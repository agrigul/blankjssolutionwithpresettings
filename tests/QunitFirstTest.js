"use strict";


QUnit.test("TestLogger item is not null", function (assert) {
    assert.ok(null !== logger, "Passed!");
});

QUnit.test("TestLogger returns correct name", function (assert) {
    var logger = new TestLogger();
    assert.equal(logger.get_name(), "name:" + logger.name, "Passed!");
});


