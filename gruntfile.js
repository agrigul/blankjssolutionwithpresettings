module.exports = function (grunt) {
    "use strict"
    // Задачи
    grunt.initConfig({


        // копируем библиотеки
        copy: {
            bower_components: {
                files: [
                    // Production version
                    {
                        expand: true,
                        flatten: true,
                        cwd: '',
                        src: ['index.html'],
                        dest: "build/prod/"
                    },
                    {
                        expand: true,
                        flatten: true,
                        cwd: 'bower_components/jquery/dist/',
                        src: ['*.min.*'],
                        dest: "build/prod/jquery/"
                    },
                    {
                        expand: true,
                        flatten: true,
                        cwd: 'bower_components/bootstrap/dist/',
                        src: ['*/*.min.*'],
                        dest: "build/prod/bootstrap/"
                    },

                    //Development version
                    {
                        expand: true,
                        flatten: true,
                        cwd: 'bower_components/jquery/dist/',
                        src: ['*.js', '!*.min.*'],
                        dest: "src/libs/jquery/"
                    },
                    {
                        expand: true,
                        flatten: true,
                        cwd: 'bower_components/bootstrap/dist/',
                        src: ['*/*.js', '*/*.css', '*/*.map', '!*/*.min.*', '!*/npm.js'],
                        dest: "src/libs/bootstrap/"
                    }
                ]
            }
        },


        // Склеиваем
        concat: {
            options: {
                separator: ';'
            },
            libs: {
                src: 'src/libs/**/*.js',
                dest: 'build/temp/libs.js'
            },
            main: {
                src: [
                    'src/app/**/*.js'  /// Все JS-файлы в папке
                ],
                dest: 'build/temp/scripts.js'
            }
        },

        // Сжимаем наши скрипты
        uglify: {
            libs: {
                files: {
                    // Результат задачи concat
                    'build/prod/libs.min.js': '<%= concat.libs.dest %>'
                }
            },
            main: {
                files: {
                    // Результат задачи concat
                    'build/prod/scripts.min.js': '<%= concat.main.dest %>'
                }
            }
        },


        // собираем css из less файлов
        less: {
            build: {
                src: 'src/css/*.less',
                dest: 'build/temp/css/style.css'
            }

        },

        // минимизируем css
        cssmin: {
            options: {
                sourceMap: false
            },
            target: {
                files: [{
                    expand: true,
                    cwd: 'build/temp/css',
                    src: ['style.css', '!*.min.css'],
                    dest: 'build/prod/css',
                    ext: '.min.css'
                }]
            }
        },


        /*jshint: {
         files: ['TestFactory.js', 'src/!**!/!*.js', 'test/!**!/!*.js'],
         options: {
         globals: {
         jQuery: true
         }
         }
         },*/
        watch: {
            concat: {
                files: '<%= concat.main.src %>',
                tasks: ['concat']
            }

        },

        useminPrepare: {
            html: 'index.html',
            options: {
                dest: 'build/prod'
            }
        },

        usemin: {html: ['<%= useminPrepare.options.dest %>/index.html']}


    });

    // Загрузка плагинов, установленных с помощью npm install
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-usemin');
    grunt.loadNpmTasks('grunt-contrib-watch');

    // Задача по умолчанию
    grunt.registerTask('default', ['less', 'cssmin', 'copy', 'concat', 'uglify', 'watch']);
    grunt.registerTask('prod', ['less', 'cssmin', 'copy', 'concat', 'uglify', 'usemin']);

    //initConfiggrunt.initConfig({
    //    jshint: {
    //        files: ['TestFactory.js', 'src/**/*.js', 'test/**/*.js'],
    //        options: {
    //            globals: {
    //                jQuery: true
    //            }
    //        }
    //    },
    //    watch: {
    //        files: ['<%= jshint.files %>'],
    //        tasks: ['jshint']
    //    }
    //});
    //
    //grunt.loadNpmTasks('grunt-contrib-jshint');
    //grunt.loadNpmTasks('grunt-contrib-watch');
    //
    //grunt.registerTask('default', ['jshint']);module

};