/**
 * Created by agriguletskiy on 03.07.2015.
 */
function TestLogger() {
    "use strict";

    this.name = "TestLogger";

    TestLogger.prototype.get_name = function () {
        return "name:" + this.name;
    };

    TestLogger.prototype.set_name = function (name) {
        this.name = name;
    };
}

var logger = new TestLogger();
